FROM node:16-alpine

WORKDIR /app
ADD package.json package-lock.json /app/

COPY . .

RUN npm install

CMD [ "npm", "start" ]