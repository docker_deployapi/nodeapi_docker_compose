var express = require('express');
var router = express.Router();
const { exec } = require("child_process");
var sleep = require('sleep');

router.get('/path', function(req, res, next) {
  exec("ls "+ process.env.DOCKER_PATH_SET + "&& pwd "+ process.env.DOCKER_PATH_SET, (error, stdout, stderr) => {
      if (error) {
          console.log((`error: ${error.message}`));
          return;
      }
      if (stderr) {
          res.send((`stderr: ${stderr}`));
          return;
      }
    //   console.log(`stdout: ${stdout}`);
    //   res.send(`stdout: ${stdout}`);
      res.json({ status: 200, stdout: `${stdout}` })
  });
});

router.get('/version', function(req, res, next) {
    exec("docker-compose version", (error, stdout, stderr) => {
        if (error) {
            console.log((`error: ${error.message}`));
            return;
        }
        if (stderr) {
            res.send((`stderr: ${stderr}`));
            return;
        }
        // console.log(`stdout: ${stdout}`);
        res.json({ status: 200, stdout: `${stdout}` })
    });
});

router.get('/composeChecking', function(req, res, next) {
    exec("cat " + process.env.DOCKER_PATH_SET + "/docker-compose.yml", (error, stdout, stderr) => {
        if (error) {
            console.log((`error: ${error.message}`));
            res.status(500);
        }
        if (stderr) {
            res.send((`stderr: ${stderr}`));
        }
        // console.log(`stdout: ${stdout}`);
        res.status(200).send(`stdout: ${stdout}`);
    });
});

router.get('/composeUp', function(req, res, next) {
    exec("docker-compose -f " + process.env.DOCKER_PATH_SET + "/docker-compose.yml up -d", (error, stdout, stderr) => {
        if (error) {
            console.log((`error: ${error.message}`));
            res.status(500);
        }
        // console.log(`stdout: ${stdout}`);
        // sleep.sleep(10)
        res.status(200).send("docker-compose deploy waiting minute time");
    });
});

router.get('/composeUpBuild', function(req, res, next) {
    exec("docker-compose -f " + process.env.DOCKER_PATH_SET + "/docker-compose.yml up -d --build", (error, stdout, stderr) => {
        if (error) {
            console.log((`error: ${error.message}`));
            res.status(500);
        }
        // console.log(`stdout: ${stdout}`);
        // sleep.sleep(10)
        res.status(200).send("docker-compose build and deploy waiting minute time");
    });
});

router.get('/composeDown', function(req, res, next) {
    exec("docker-compose -f " + process.env.DOCKER_PATH_SET + "/docker-compose.yml down", (error, stdout, stderr) => {
        if (error) {
            console.log((`error: ${error.message}`));
            res.status(500);
        }
        // console.log(`stdout: ${stdout}`);
        // sleep.sleep(10)
        res.status(200).send(`stdout: docker-compose service is !!Down!! waiting minute time`);
    });
});

router.get('/composeRestart', function(req, res, next) {
    exec("docker-compose -f " + process.env.DOCKER_PATH_SET + "/docker-compose.yml restart", (error, stdout, stderr) => {
        if (error) {
            console.log((`error: ${error.message}`));
            res.status(500);
        }
        // console.log(`stdout: ${stdout}`);
        // sleep.sleep(10)
        res.status(200).send(`stdout: docker-compose service is !!Restart!! waiting minute time`);
    });
});

router.get('/composeStatus', function(req, res, next) {
    exec("docker-compose -f " + process.env.DOCKER_PATH_SET + "/docker-compose.yml ps | gawk 'NR!=1 {print $7}'", (error, stdout, stderr) => {
        if (error) {
            console.log((`error: ${error.message}`));
            res.status(500);
        }
        if (stderr) {
            res.send((`stderr: ${stderr}`));
        }
        // console.log(`stdout: ${stdout}`);
        res.status(200).send(`stdout: status docker-compose is ${stdout}`);
    });
});

router.get('/composeTime/:service', function(req, res, next) {
    let service_name = req.params.service;
    exec("docker ps | grep " + service_name +" | gawk '{print $10$11$12}'", (error, stdout, stderr) => {
        if (error) {
            console.log((`error: ${error.message}`));
            res.status(500);
        }
        if (stderr) {
            res.send((`stderr: ${stderr}`));
        }
        // console.log(`stdout: ${stdout}`);
        res.status(200).send(`stdout: time update container name ${service_name} is ${stdout}`);
    });
});

module.exports = router;
