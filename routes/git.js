var express = require('express');
var router = express.Router();
const { exec } = require("child_process");

router.get('/status', function(req, res, next) {
    exec("git -C "+ process.env.DOCKER_PATH_SET +" status", (error, stdout, stderr) => {
        if (error) {
            console.log((`error: ${error.message}`));
            return;
        }
        if (stderr) {
            res.send((`stderr: ${stderr}`));
            return;
        }
        // console.log(`stdout: ${stdout}`);
        res.status(200).send(`stdout: ${stdout}`);
    });
});

router.get('/update/:branch', function(req, res, next) {
    var branch = req.params.branch;
    exec("git -C "+ process.env.DOCKER_PATH_SET +" pull origin "+ branch, (error, stdout, stderr) => {
        if (error) {
            console.log((`error: ${error.message}`));
            return;
        }
        if (stderr) {
            res.send((`stderr: ${stderr}`));
            return;
        }
        // console.log(`stdout: ${stdout}`);
        res.status(200).send(`stdout: ${stdout}`);
    });
});

module.exports = router;
